'use strict';

const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

const SRC = path.join(__dirname, '/src');
const DIST = path.join(SRC, '/dist');
const FRONT = path.join(SRC, '/front');

module.exports = {
  entry: path.join(FRONT, '/main.js'),
  output: {
    path: DIST,
    filename: 'bundle.js'
  },
  module: {
   loaders: [
      { test: /\.js?$/, exclude: /node_modules/, loader: 'babel?cacheDirectory' },
      { test: /\.less$/, loader: 'style!css!less' },
      { test: /\.png$/, loader: 'url-loader?mimetype=image/png' },

      // bootstrap-webpack has access to the jQuery object
      { test: /bootstrap\/js\//, loader: 'imports?jQuery=jquery' },
      // Needed for the css-loader when bootstrap-webpack loads bootstrap's css.
      { test: /\.(woff|woff2)$/,   loader: 'url?limit=10000&mimetype=application/font-woff' },
      { test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,    loader: 'url?limit=10000&mimetype=application/octet-stream' },
      { test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,    loader: 'file' },
      { test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,    loader: 'url?limit=10000&mimetype=image/svg+xml' }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'ReactReduxPresentation',
      template: path.join(FRONT, 'index.ejs'),
      inject: 'body'
    }),
    new webpack.NoErrorsPlugin()
  ],
  devServer: {
    hot: true,
    inline: true,
    contentBase: SRC
  },
  devtool: 'source-map'
};
