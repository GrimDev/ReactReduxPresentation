'use strict';

export const NEXT_SLIDE = 'NEXT_SLIDE';
export const PREV_SLIDE = 'PREV_SLIDE';
export const SET_SLIDES = 'SET_SLIDES';
