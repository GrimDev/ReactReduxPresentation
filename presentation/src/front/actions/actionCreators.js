'use strict';

import * as actions from './actionTypes';

export const setSlides = (slides) => {
  return {
    type: actions.SET_SLIDES,
    slides
  };
};

export const nextSlide = () => {
  return {
    type: actions.NEXT_SLIDE
  };
};

export const prevSlide = () => {
  return {
    type: actions.PREV_SLIDE
  };
};
