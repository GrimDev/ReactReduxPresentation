'use strict';

import * as actionTypes from '../actions/actionTypes';

export const slides = (state = {
  currentSlide: 0,
}, action) => {
  switch(action.type) {
    case actionTypes.NEXT_SLIDE:
      return {
        ...state,
        currentSlide: state.currentSlide + 1
      };
      break;
    case actionTypes.PREV_SLIDE:
      return {
        ...state,
        currentSlide: state.currentSlide - 1
      };
      break;
    case actionTypes.SET_SLIDES:
      return {
        ...state,
        slides: action.slides
      };
      break;
    default:
      return state;
      break;
  }
};
