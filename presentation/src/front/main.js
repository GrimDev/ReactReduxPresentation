'use strict';

require('bootstrap-webpack');
require('./style/main.less');

import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, combineReducers } from 'redux';
import { Provider } from 'react-redux';

import * as reducers from './reducers';
import Presentation from './containers/Presentation';

const reducer = combineReducers({...reducers});
const store = createStore(reducer);

ReactDOM.render(
  <Provider store={store}>
    <Presentation />
  </Provider>,
  document.getElementById('root')
);
