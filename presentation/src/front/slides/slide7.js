'use strict';

import React from 'react';

export default {
  title: 'Angular',
  content: (
    <div className="slide">
      <div id="content">
        <div id="slide7">
          <h1>Architecture d'une application Angular</h1>
          <img id="redux-diagram" src={require('../img/agoodangularapp.png')} alt="redux-diagram" />
        </div>
      </div>
    </div>
  )
};
