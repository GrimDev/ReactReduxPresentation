'use strict';

import React from 'react';

export default {
  title: 'React Redux',
  content: (
    <div className="slide">
      <div id="content">
        <div id="slide8">
          <h1>Architecture d'une application React Redux</h1>
          <img id="redux-diagram" src={require('../img/apgoodreactreduxapp.png')} alt="redux-diagram" />
        </div>
      </div>
    </div>
  )
};
