'use strict';

import React from 'react';

import ReactForm from '../containers/slideComponents/reactForm';

export default {
  title: 'React component',
  content: (
    <div className="slide">
      <div id="content">
        <div id="slide2">
          <div className="left">
            <ReactForm />
          </div>
          <div className="right">
            <ul>
              <li>Autonome</li>
              <li>Auto validé</li>
              <li>Facilement testable</li>
              <li>Peut contenir des components</li>
              <li>Evènements liés à celui-ci</li>
              <ul>
                <li>Component[Will/Did]Mount</li>
                <li>Component[Will/Did]ReceiveProps</li>
                <li>...</li>
              </ul>
            </ul>
          </div>
        </div>
      </div>
    </div>
  )
};
