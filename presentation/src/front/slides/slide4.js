'use strict';

import React from 'react';

export default {
  title: 'Redux',
  content: (
    <div className="slide">
      <div id="content">
        <div id="slide4">
          <h1>"Redux is a predictable state container for JavaScript apps."</h1>
          <img id="redux-diagram" src={require('../img/redux-diagram.png')} alt="redux-diagram" />
        </div>
      </div>
    </div>
  )
};
