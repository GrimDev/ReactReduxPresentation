'use strict';

const NUMBER_OF_SLIDES = 10;
const SLIDE_COLORS = '747271';

let slides = [];

for(let i = 0; i < NUMBER_OF_SLIDES; i++) {
  let slide = require(`./slide${i}`);
  if(i > 1) {
    slides[i - 1].next = slide;
  }
  slide.color = SLIDE_COLORS;
  slides.push(slide);
}

export default slides;
