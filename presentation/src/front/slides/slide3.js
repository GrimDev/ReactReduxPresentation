'use strict';

import React from 'react';

export default {
  title: 'Virtual DOM',
  content: (
    <div className="slide">
      <div id="content">
        <div id="slide3">
          <div className="left">
            <img id="virtual-dom" src={require('../img/virtual_dom.png')} alt="VirtualDom" />
          </div>
          <div className="right">
            <ul>
              <li>Abstraction du DOM</li>
              <li>Calculs sur le DOM virtuel</li>
              <li>Optimisation</li>
              <li>Merge sur le DOM réel</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  )
};
