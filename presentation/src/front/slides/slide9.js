'use strict';

import React from 'react';

export default {
  title: 'Angular VS React/Redux',
  content: (
    <div className="slide">
      <div id="content">
        <div id="slide9">
          <img id="redux-diagram" src={require('../img/angularvsreact.png')} alt="redux-diagram" />
        </div>
      </div>
    </div>
  )
};
