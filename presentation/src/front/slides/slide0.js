'use strict';

import React from 'react';

export default {
  title: '',
  content: (
    <div className="slide">
      <img id="react-main-title" src={require('../img/react-logo.png')} alt="ReactLogo" />
      <div id="redux-main-title">
        <h1>React & Redux</h1>
      </div>
      <div id="main-subtitle">Kevin Cousin - Full stack programmer</div>
      <img id="title-expandium-logo" src={require('../img/expandium.png')} alt="ExpandiumLogo" />
    </div>
  )
};
