'use strict';

import React from 'react';

export default {
  title: 'Redux state',
  content: (
    <div className="slide">
      <div id="content">
        <div id="slide5">
          <h1>"Un état modifié par des actions"</h1>
          <img id="redux-diagram" src={require('../img/reduxState.png')} alt="redux-diagram" />
        </div>
      </div>
    </div>
  )
};
