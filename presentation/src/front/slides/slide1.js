'use strict';

import React from 'react';

export default {
  title: 'React ?',
  content: (
    <div className="slide">
      <div id="content">
        <div id="left">
          <img id="react-logo-extended" src={require('../img/react-logo-extended.png')} alt="ReactLogo" />
        </div>
        <div id="facts">
          <ul>
            <li>Une librarie Javascript</li>
            <li>Uniquement pour les UI</li>
            <li>Orienté composant</li>
            <li>Optimise les affichages temps réel</li>
          </ul>
        </div>
      </div>
    </div>
  )
};
