'use strict';

import React, { Component } from 'react';

import ReactInput from './reactInput';
import ReactBtnGroup from './reactBtnGroup';

class ReactForm extends Component {
  render() {
    const btns = [
      <button className="btn btn-success">Button1</button>,
      <button className="btn btn-warning">Button2</button>
    ];
    return (
      <form className="react_component1">
        <ReactInput inputId="input1" label="Input 1"/>
        <ReactInput inputId="input2" label="Input 2"/>
        <ReactBtnGroup btns={btns}/>
      </form>
    );
  }
};

export default ReactForm;
