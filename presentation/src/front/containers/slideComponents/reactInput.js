'use strict';

import React, { Component, PropTypes } from 'react';

class ReactInput extends Component {
  constructor(props) {
    super(props);
    this.state = {value: 'Hello!', color: 'white'};
  }
  static get propTypes() {
    return {
      inputId: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired,
      value: PropTypes.string
    };
  }
  handleChange(event) {
    const newValue = event.target.value;
    const newColor = newValue === 'Hello!' ? 'white' : 'pink';
    this.setState({value: newValue, color: newColor});
  }
  render() {
    const { inputId, label } = this.props;
    const { color, value } = this.state;
    const style = {
      backgroundColor: color
    };
    return (
      <div className="form-group react_component2">
        <label htmlFor={inputId}>{label}</label>
        <input
          id={inputId}
          className="form-control"
          type="text"
          style={style}
          value={value}
          onChange={this.handleChange.bind(this)}
          />
      </div>
    );
  }
}

export default ReactInput;
