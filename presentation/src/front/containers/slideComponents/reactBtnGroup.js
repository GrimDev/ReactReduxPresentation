'use strict';

import React, { Component, PropTypes } from 'react';

class ReactForm extends Component {
  static get propTypes() {
    return {
      btns: PropTypes.array
    };
  }
  render() {
    const { btns } = this.props;
    return (
      <div className="btnList react_component3">
        {btns.map((btn, key) => {
          return <span key={key}>{btn}</span>;
        })}
      </div>
    );
  }
}

export default ReactForm;
