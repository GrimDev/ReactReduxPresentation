'use strict';

import * as _ from 'lodash';
import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import { setSlides, nextSlide, prevSlide } from '../actions/actionCreators';

import slides from '../slides';

const nextKeyCodes = [39, 32, 13];
const prevKeyCode = 37;

const previous = (props) => {
  if(props.currentSlide > 0) {
    props.dispatch(prevSlide());
  }
};
const next = (props) => {
  if(props.currentSlide < props.slides.length - 1) {
    props.dispatch(nextSlide());
  }
};

const keyPressed = _.curry((props, event) => {
  if(_.includes(nextKeyCodes, event.keyCode)) {
    next(props);
  } else if (event.keyCode === prevKeyCode) {
    previous(props);
  }
});

class Presentation extends Component {
  constructor(props) {
    super(props);
  }
  static get propTypes() {
    return {
      currentSlide: PropTypes.number,
      slides: PropTypes.array,
      dispatch: PropTypes.func
    };
  }
  componentDidMount() {
    this.props.dispatch(setSlides(slides));
  }
  render() {
    const { currentSlide } = this.props;
    document.onkeydown = keyPressed(this.props);
    return (
      <div>
        <h1 className="title">
          {slides[currentSlide] && slides[currentSlide].default.title}
        </h1>
        <div className="content">
          {slides[currentSlide] && slides[currentSlide].default.content}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    currentSlide: state.slides.currentSlide,
    slides: state.slides.slides
  };
};

export default connect(mapStateToProps)(Presentation);
